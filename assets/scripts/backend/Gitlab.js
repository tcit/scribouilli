//@ts-check

import parseMarkdown from "@github-docs/frontmatter";

export default class Gitlab {

  constructor(accessToken, instance) {
    this.accessToken = accessToken
    this.instance = instance
    this.commitsEtag = undefined
    this.latestCommit = undefined
    this.getFilesCache = new Map()
    this.fileCached = undefined
    this.customCSSPath = "assets/css/custom.css"
  }

  getAuthenticatedUser() {
    return this.callGitlabAPI("/user").then((response) => {
      return response.json()
    })
  }

  getRepository(login, repoName) {
    return this.callGitlabAPI(`https://api.github.com/repos/${login}/${repoName}`).then((response) => {
      return response.json()
    }).catch(msg => {
      if (msg === "NOT_FOUND") {
        throw "REPOSITORY_NOT_FOUND"
      }

      throw msg
    })
  }

  /**
   * @summary Get file informations
   * @param {string} login
   * @param {string} repoName
   * @param {string} fileName
   * @returns
   */
  getFile(login, repoName, fileName) {
    const repositoryIdOrPath = encodeURIComponent(`${login}/${repoName}`)
    return this.callGitlabAPI(`/projects/${repositoryIdOrPath}/repository/files/${fileName}`, {
      headers: {
        Authorization: "token " + this.accessToken,
        "If-None-Match": this.getFilesCache.get(fileName)?.etag
      }
    }).then((httpResp) => {
      if (httpResp.status !== 304) {
        const file = httpResp.json()
        this.getFilesCache.set(fileName, {etag: httpResp.headers.get("etag"), file: file}) 
      }
      return this.getFilesCache.get(fileName).file
    })
  }

  getGitHubPagesSite(login, repoName) {
    return this.callGitlabAPI(`https://api.github.com/repos/${login}/${repoName}/pages`).then((response) => {
      return response.json()
    })
  }

  getDeploymentStatus(deployment) {
    return this.callGitlabAPI(deployment.statuses_url).then((response) => {
      return response.json()
    })
  }

  /**
  * @summary Remove file from github
  */
  deleteFile(login, repoName, fileName, sha) {
    return this.callGitlabAPI(
      `/projects/${login}/${repoName}/repository/files/${fileName}`,
      {
        headers: { Authorization: "token " + this.accessToken },
        method: "DELETE",
        body: JSON.stringify({
          sha,
          message: `suppression du fichier ${fileName}`,
        }),
      }
    ).then((response) => {
      return response.json()
    })
  }

  deleteRepository(login, repoName) {
    return this.callGitlabAPI(
      `DELETE /projects/${login}/${repoName}`,
      {
        headers: { Authorization: "token " + this.accessToken },
        method: "DELETE",
      })
  }

  /**
   * @summary Create a file
   */
  createFile(login, repoName, fileName, body) {
    return this.callGitlabAPI(
      `/projects/${login}/${repoName}/repository/files/${fileName}`,
      {
        headers: { Authorization: "token " + this.accessToken },
        method: "PUT",
        body: JSON.stringify(this.bodyToGitlab(body)),
      }
    )
  }

  createCustomCSS(login, repoName, content) {
    return this.createFile(login, repoName, this.customCSSPath,
      {
        commit_message: "création du ficher de styles custom",
        content: content
      }
    )
  }

  /**
   * @summary Update a file
   *
   * On supprime la référence correspondante à l'ancien nom
   * Nous ne pouvons pas renommer le fichier via l'API
   * https://stackoverflow.com/questions/31563444/rename-a-file-with-github-api
   *
   * @param {string} login
   * @param {string} repoName
   * @param {string} oldfileName Name of the file
   * @param {string} newFileName Name of the file after modification
   * @param {*} body
   * @param {string} sha Sha of the file generated by Github
   * @returns
   *
   */
  updateFile(login, repoName, oldfileName, newFileName, body, sha) {
    if (newFileName === oldfileName) {
      body.branch = 'main'

      return this.callGitlabAPI(
        `/projects/${login}/${repoName}/repository/files/${oldfileName}`,
        {
          headers: { Authorization: "token " + this.accessToken },
          method: "PUT",
          body: JSON.stringify(this.bodyToGitlab(body)),
        }
      )
    } else {
      return this.deleteFile(login, repoName, oldfileName, sha)
        .then(() => {
          return this.createFile(login, repoName, newFileName, body)
        })
    }
  }

  updateCustomCSS(login, repoName, content, sha) {
    return this.updateFile(login, repoName, this.customCSSPath, this.customCSSPath, {
      content: content,
      commit_message: "mise à jour du thème"
    }, sha).then(response => {
      return response.json()
    })
  }

  getLatestCommit(login, repoName) {
    return this.callGitlabAPI(`/projects/${login}/${repoName}/repository/commits`, {
      headers: {
        Authorization: "token " + this.accessToken,
        "If-None-Match": this.commitsEtag
      }
    }).then((httpResp) => {
      if (httpResp.status === 304) {
        return [this.latestCommit]
      } else {
        this.commitsEtag = httpResp.headers.get("etag")
        return httpResp.json()
      }
    }).then(commits => {
      this.latestCommit = commits[0]
      return this.latestCommit
    })
  }

  getPagesList(login, repoName) {
    return this.getLatestCommit(login, repoName).then(({ sha }) => {
      return this.callGitlabAPI(`/projects/${login}/${repoName}/repository/files/${sha}`)
        .then(response => response.json())
        .then(({ tree }) => {
          const pageFiles = tree.filter((f) => {
            return (
              f.type === "blob" &&
              (f.path.endsWith(".md") || f.path.endsWith(".html"))
            );
          });
          return pageFiles;
        }
        ).then((files) => {
          const pagePs = files.map((file) => {
            return this.getFile(login, repoName, file.path)
              .then((page) => {
                const { data,  content: markdownContent } = parseMarkdown(Buffer.from(page.content, "base64").toString());
                const title = data?.title;
                return { title: title, path: file.path, content: markdownContent }
              }).catch(() => {
                return { title: file.path, path: file.path, content: "" }
              })
          })
          return Promise.all(pagePs)
        });
    });
  }

  getLastDeployment(login, repoName) {
    return this.callGitlabAPI(`/projects/${login}${repoName}/pipelines/latest`).then((deployments) => deployments[0])
  }

  /**
   * @summary This method must be called for each API call.
   *
   * It handles access_token errors
   *
   */
  callGitlabAPI(url, requestParams = { headers: { Authorization: "token " + this.accessToken } }) {
    return fetch(`https://${this.instance}/api/v4${url}`, requestParams).then((httpResp) => {
      if (httpResp.status === 404) {
        throw "NOT_FOUND"
      }

      if (httpResp.status === 401) {
        this.accessToken = undefined
        console.debug("this accessToken : ", this)
        throw "INVALIDATE_TOKEN"
      }
      return httpResp
    })
  }

  bodyToGitlab(body) {
    return { ...body, commit_message: body.message }
  }
}
